# DDCM

A collection of numerical tools for Data-Driven Computational Mechanics.

Created by Erik Prume at the Institute of Applied Mechanics, Prof. Stefanie Reese, RWTH Aachen University.

## Introduction

<p>
For a background on data-driven computational mechanics we refer to

<li><cite>
Kirchdoerfer, T., & Ortiz, M. (2016). Data-driven computational mechanics. Computer Methods in Applied Mechanics and Engineering, 304, 81-101.
</cite></li>

</p>


<p>

For the idea behind the non-intrusive coupling to FEM-environments we refer to

<li><cite>
Prume, E., Stainier, L., Ortiz, M., & Reese, S. (2023). A data‐driven solver scheme for inelastic problems. PAMM, 23(1), e202200153. https://doi.org/10.1002/pamm.202200153
</cite></li>

If you are using this library, please cite this article.

</p>

<p>

Within this framework, two essential operations are involved:
 <ul>
  <li>For a given stress field, compute the closest stress field which satisfies mechanical equilibrium</li>
  <li>For a given strain field, compute the closest strain field which satisfies kinematical compatibility</li>
</ul> 
</p>
<p>
This repository provides these two operations using the open-source FEM tool FEniCSx by

 <ul>
  <li>  
<code>equilibrium_stress_field = project_stress(stress_field)</code>
</li>
  <li>
<code>compatible_strain_field = project_strain(strain_field)</code>
  </li>
</ul> 
</p>
For the details, we refer to the example <code>fenicsx_input_example.ipynb</code>.

<br>

Other operations related to the method are not (yet) included in this repository. 


## Installation

For the projection operations, only FEniCS / dolfinx (https://github.com/FEniCS/dolfinx) is required.

The recommended way is the installation using conda.

Within your (possibly empty) conda environment, install  FEniCS / dolfinx with:

```
conda install -c conda-forge fenics-dolfinx mpich pyvista
```
