#created by Erik Prume, Institute of Applied Mechanics (Stefanie Reese), RWTH Aachen University

import numpy as np
import ufl

from petsc4py.PETSc import ScalarType
from petsc4py import PETSc

from dolfinx import fem

class DDfenicsx:

    def __init__(self,domain, V,Vdata,bc_raw, T, cm):

        self.domain = domain
        self.V = V
        self.Vdata = Vdata

        self.nmp = self.Vdata.tabulate_dof_coordinates().shape[0] #number of material points
        self.ndm = self.Vdata.num_sub_spaces # stress/strain dimension
        self.nnodes = self.V.tabulate_dof_coordinates().shape[0] #number of nodes

        self.cm = cm #metric coefficient
        self.load_scale = 1.0

        #DISPLACEMENT PROBLEM
        self.bc = []
        for (bci, dofsi, dir) in bc_raw:
            if dir==-1: #dir==-1 means bc applied to all directions
                self.bc.append(fem.dirichletbc(bci, dofsi, self.V))
            else:
                self.bc.append(fem.dirichletbc(bci, dofsi, self.V.sub(dir)))

        T_U = []
        self.LP_U = LinearProblem(self.domain,self.V,self.Vdata, self.bc, T_U,self.cm)

        self.LP_U.u_h.name = "Displacement"
        self.LP_U.data.name = "Strain"

        #STRESS PROBLEM  
        self.V2 = self.V.clone()
        #boundary conditions
        self.bc2 = []
        for (bci, dofsi, dir) in bc_raw:
            u_0 = np.zeros_like(bci, dtype=ScalarType)
            if dir==-1:
                self.bc2.append(fem.dirichletbc(u_0, dofsi, self.V2))
            else:
                self.bc2.append(fem.dirichletbc(u_0, dofsi, self.V2.sub(dir)))

        #applied forces
        self.T = T

        self.LP_ETA = LinearProblem(self.domain,self.V2,self.Vdata, self.bc2, self.T,self.cm) 

        self.LP_ETA.u_h.name = "Lagrange_Multiplier"
        self.LP_ETA.data.name = "Stress"

    def project_strain(self,eps0):
        #eps0 dimensions: number of material points (self.nmp) x dimension strain (self.ndm)

        #alternative:
        # def data_fct(x):
        #     return self.cm @ eps0.T
        # self.LP_U.data.interpolate(data_fct)

        self.LP_U.data.x.array[:] = (eps0 @ self.cm).flatten()

        self.LP_U.solve(self.load_scale)

        eps_t  = epsilon(self.LP_U.u_h)

        eps_expr = fem.Expression(eps_t,self.Vdata.element.interpolation_points())
        self.LP_U.data.interpolate(eps_expr)

        return np.array(self.LP_U.data.x.array.reshape(-1,self.ndm))

    def compute_strain_field(self, u):
        #u dimensions: number of nodes (self.nnodes) x dimension space (3)

        self.LP_U.u_h.x.array[:] = u.flatten()
        eps_t = epsilon(self.LP_U.u_h)

        eps_expr = fem.Expression(eps_t,self.Vdata.element.interpolation_points())
        self.LP_U.data.interpolate(eps_expr)


        return np.array(self.LP_U.data.x.array.reshape(-1,self.ndm))


    def get_displacements(self,eps0):

        self.LP_U.data.x.array[:] = (eps0@self.cm).flatten()
        
        self.LP_U.solve(self.load_scale)

        return np.array(self.LP_U.u_h.x.array.reshape(-1,3))

    def project_stress(self,sig0):
        #sig0 dimensions: number of material points (self.nmp) x dimension stress (self.ndm)

        self.LP_ETA.data.x.array[:] = (- sig0).flatten()

        self.LP_ETA.solve(self.load_scale)

        sig_t = sigma(self.cm,self.LP_ETA.u_h)

        sig_expr = fem.Expression(sig_t,self.Vdata.element.interpolation_points())
        self.LP_ETA.data.interpolate(sig_expr)

        return np.array(sig0 + self.LP_ETA.data.x.array.reshape(self.nmp,self.ndm))
    
    def residual_force_vector(self,sig0):


        sig_fun = fem.Function(self.LP_ETA.Vdata)
        sig_fun.x.array[:] = sig0.flatten()
        L_temp = ufl.inner(sig_fun, epsilon(self.LP_ETA.v)) * ufl.dx
        L = fem.form(L_temp)
        div_sig = fem.petsc.create_vector(L)

        with div_sig.localForm() as loc_b:
            loc_b.set(0.0)
        fem.petsc.assemble_vector(div_sig, L)
        fem.set_bc(div_sig, self.LP_ETA.bc, scale =self.load_scale)

        return div_sig.getArray().reshape(-1,3)
    
    #TODO
    # def reaction_force(self,ds):
        # sig_t = sigma(self.cm,self.LP_ETA.u_h)
        # R = fem.assemble_scalar(sig_t * ds) ???

    def set_load_scale(self,val):
        self.load_scale = val

    def plot(self,xdmf,eps0,sig0,time):
        #plot domain outside and before calling this function 

        self.get_displacements(eps0) #sets clostest displacements field for eps0

        def eps_fct(x):
            return eps0.T
        def sig_fct(x):
            return sig0.T

        # self.LP_U.data.interpolate(eps_fct)
        self.LP_U.data.x.array[:] = eps0.flatten()
        self.LP_ETA.data.interpolate(sig_fct)

        xdmf.write_function(self.LP_U.u_h,t=time)
        xdmf.write_function(self.LP_U.data,t=time)
        xdmf.write_function(self.LP_ETA.data,t=time)
        # xdmf.write_function(f,t=time)

class LinearProblem:
    def __init__(self,domain,V,Vdata,bc,T, cm):

        self.domain = domain
        self.V = V
        self.Vdata = Vdata
        self.u = ufl.TrialFunction(self.V)
        self.v = ufl.TestFunction(self.V)
        self.bc = bc
        self.T = T
        self.cm = cm

        self.data = fem.Function(self.Vdata)

        L_temp = ufl.inner(self.data, epsilon(self.v)) * ufl.dx# + ufl.dot(self.T, self.v) * self.ds)
        for (Ti,dsi) in self.T:
            L_temp +=  ufl.dot(Ti, self.v) * dsi
        self.L = fem.form(L_temp)

        self.a = fem.form(ufl.inner(sigma(self.cm,self.u), epsilon(self.v)) * ufl.dx)
        self.A = fem.petsc.assemble_matrix(self.a, bcs=self.bc)
        self.A.assemble()

        self.b = fem.petsc.create_vector(self.L)
        self.u_h = fem.Function(self.V)

        #following https://jsdokken.com/dolfinx-tutorial/chapter2/heat_code.html
        self.solver = PETSc.KSP().create(self.domain.comm)
        self.solver.setOperators(self.A)
        self.solver.setType(PETSc.KSP.Type.PREONLY)
        self.solver.getPC().setType(PETSc.PC.Type.LU)

    def solve(self,load_scale):
        with self.b.localForm() as loc_b:
            loc_b.set(0.0)
        fem.petsc.assemble_vector(self.b, self.L)

        # Apply Dirichlet boundary condition to the vector
        fem.petsc.apply_lifting(self.b, [self.a], [self.bc], scale = load_scale)
        self.b.ghostUpdate(addv=PETSc.InsertMode.ADD_VALUES, mode=PETSc.ScatterMode.REVERSE)
        fem.petsc.set_bc(self.b, self.bc, scale =load_scale)

        self.solver.solve(self.b, self.u_h.vector)
        self.u_h.x.scatter_forward()

def div_sig(sig):
    return ufl.as_vector([sig[0].dx(0) + sig[3].dx(1) + sig[4].dx(2), 
                          sig[3].dx(0) + sig[1].dx(1) + sig[5].dx(2), 
                          sig[4].dx(0) + sig[5].dx(1) + sig[2].dx(2)])

def epsilon(u):
    return ufl.as_vector([u[0].dx(0), 
                    u[1].dx(1),
                    u[2].dx(2),
                    (u[0].dx(1) + u[1].dx(0)),
                    (u[0].dx(2) + u[2].dx(0)),
                    (u[1].dx(2) + u[2].dx(1))])

def sigma(cm, u):
    return ufl.as_matrix(cm) * epsilon(u)
